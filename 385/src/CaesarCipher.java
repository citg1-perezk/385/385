public class CaesarCipher {
    public static String alphabet="abcdefghijklmnopqrstuvwxyz";
    public static String EncryptCaesar(String input) {
        input=input.toLowerCase();
//Store encryption code
        String Store = "";
//traverse
        for (int i = 0; i < input.length(); i++) {
//find the position of each input letters
            int pos = alphabet.indexOf(input.charAt(i));

// Encrypt Caesar
            int encrypt = (pos + 3) % 26;

//Cipher the alphabet and find its value
            char cipher = alphabet.charAt(encrypt);
            if(input.charAt(i)==' '){
                cipher=' ';
            }
//Store
            Store += cipher;
        }
        return Store;
    }

    public static String DecryptCaesar(String input) {
        input=input.toLowerCase();
//Store encryption code
        String Store = "";
//traverse
        for (int i = 0; i < input.length(); i++) {
//find the position of each input letters
            int pos = alphabet.indexOf(input.charAt(i));

// Encrypt Caesar
            int decrypt = (pos - 3) % 26;

//Cipher the alphabet and find its value
            if(decrypt<0){
                decrypt=alphabet.length()+decrypt;
            }

            char cipher = alphabet.charAt(decrypt);
            if(input.charAt(i)==' '){
                cipher=' ';
            }
//Store
            Store += cipher;
        }
        return Store;
    }

}
