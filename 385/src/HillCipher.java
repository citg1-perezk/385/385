public class HillCipher{
    public static void keyMatrix(String key, int keyM[][]){
        int k=0;
        for(int r=0;r<3;r++){
            for(int c=0;c<3;c++){
                keyM[r][c]=(key.charAt(k))%65;
                k++;
            }
        }
    }
    public static void encryptHillCipher(int cipherM[][], int keyM[][], int message[][]){
        for(int r=0; r<3;r++){
            for(int c=0;c<1;c++){
                cipherM[r][c]=0;
                for(int x=0;x<3;x++){
                    cipherM[r][c]+=keyM[r][x]*message[x][c];
                }
                cipherM[r][c]=cipherM[r][c] % 26;

            }
        }
    }
    public static void HillCipher(String cipher, String key){
        int [][]keyM= new int[3][3];
        keyMatrix(key,keyM);
        int [][]message=new int[3][1];

        for(int i=0;i<3;i++){
            message[i][0]=(cipher.charAt(i))%65;
        }
        int[][]cipherM=new int[3][1];
        encryptHillCipher(cipherM,keyM,message);
        String cipherText="";
        for(int i=0; i<3;i++){
            cipherText+=(char)(cipherM[i][0]+65);
        }
        System.out.println("Ciphertext: "+cipherText);
    }


}
