import java.util.*;

public class Playfair {
    public static void decryptPlayfair(String text,String key){
        char[]alphabet={'A','B','C','D','E','F','G','H','I','K','L',
                'M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
        for(int i=0;i<alphabet.length;i++){
            alphabet[i]=Character.toLowerCase(alphabet[i]);
        }
        char[]cipher_arr=text.toCharArray();
        char[]key_arr=key.toCharArray();
        char[][] matrix= new char[5][5];
        int ctr=-1;
        for(int i=0;i<key_arr.length;i++)
            for(int j=0;j<25;j++)
                if(key_arr[i]==alphabet[j]) {
                    ctr++;
                    int r= ctr/5;
                    matrix[r][ctr%5]=alphabet[j];
                    alphabet[j]='0';
                    break;
                }
        for(int i=0;i<alphabet.length;i++){
            if(alphabet[i]!='0'){
                ctr++;int r= ctr/5;
                matrix[r][ctr%5]= alphabet[i];
            }
        }
        /*System.out.println("\nThe Reference Table is:");

        for(int i=0;i<5;i++){
            for(int j=0;j<5;j++)
                System.out.print(matrix[i][j]+" ");
            System.out.println();
        }*/
        char[] random=new char[cipher_arr.length];
        int ctr1=0;
        for(int i=0;i<cipher_arr.length;i=i+2){
            int row1=0,row2=0,col1=0,col2=0;
            for(int j=0;j<5;j++){
                for(int k=0;k<5;k++)
                {
                    if(cipher_arr[i]==matrix[j][k]){
                        row1=j;
                        col1=k;
                        break;
                    }
                }
            }
            for(int j=0;j<5;j++){
                for(int k=0;k<5;k++){
                    if(cipher_arr[i+1]==matrix[j][k]){
                        row2=j;
                        col2=k;
                        break;
                    }
                }
            }
            if(row1==row2){
                col1=(col1-1+5)%5;
                col2=(col2-1+5)%5;
                random[ctr1++]=matrix[row1][col1];
                random[ctr1++]=matrix[row2][col2];
            }
            else if(col1==col2){
                row1=(row1-1+5)%5;
                row2=(row2-1+5)%5;
                random[ctr1++]=matrix[row1][col1];
                random[ctr1++]=matrix[row2][col2];}
            else if(row1!=row2 && col1!=col2){
                int row=0,col=0;
                row=row1;
                col=col2;
                random[ctr1++]=matrix[row][col];
                row=row2;
                col=col1;
                random[ctr1++]=matrix[row][col];
            }
            else{

            }
        }
       /* System.out.print("\nThe Intermediate Text is: ");
        for(int i =0;i<ctr1;i++)
           random[i]);*/
        char[] decprypt_arr=new char[100];
        int decprypt_arr_ctr=0;
        for(int i=0;i<ctr1;i++) {
            if(i==0){
                decprypt_arr[decprypt_arr_ctr++]=random[i];
                continue;
            }
            if(i==1 && random[i-1]==random[i+1] && random[i]=='X'){
                continue;
            }
            if(i==1 && random[i-1]!=random[i+1] && random[i]!='X')
            {
                decprypt_arr[decprypt_arr_ctr++]=random[i];
                continue;
            }
            if(i==2 && random[i-1]==random[i+1] && random[i]=='X'){
                continue;
            }
            if(i==2 && random[i-1]!=random[i+1] && random[i]!='X'){
                decprypt_arr[decprypt_arr_ctr++]=random[i];
                continue;
            }
            if(i!=ctr1-2 && i!=ctr1-1&& random[i-1]==random[i+1] && random[i]=='X'){
                continue;
            }
            if(i!=ctr1-2 && i!=ctr1-1 && random[i-1]==random[i+1] && random[i]!='X'){
                decprypt_arr[decprypt_arr_ctr++]=random[i];
                continue;
            }
            if(i==ctr1-2 && random[i-1]==random[i+1] && random[i]=='X'){
                continue;
            }if(i==ctr1-2 && random[i-1]==random[i+1] && random[i]!='X'){
                decprypt_arr[decprypt_arr_ctr++]=random[i];
                continue;
            }
            if(i==ctr1-1 && i%2!=0 && random[i]=='X'){
                continue;
            }if(i==ctr1-1 && random[i]!='X'){
                decprypt_arr[decprypt_arr_ctr++]=random[i];
                continue;
            }
            if(i==ctr1-1 && i%2==0 && random[i]=='X'){
                continue;
            }if(i>=0){decprypt_arr[decprypt_arr_ctr++]=random[i];
                continue;
            }
        }
        System.out.print("\n\nThe Decrypted Text is: ");
        for(int i=0;i<decprypt_arr_ctr;i++)
            System.out.print(decprypt_arr[i]);
    }
    }






