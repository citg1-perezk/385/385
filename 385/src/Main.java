import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
       /* RailFenceCipher railFenceCipher= new RailFenceCipher();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter ciphertext: ");
        String cipherText = scanner.nextLine();
        System.out.print("Enter number of rails: ");
        int key = scanner.nextInt();
        String ciphertext = railFenceCipher.decryptRailFence(cipherText, key);
        System.out.println("Ciphertext: " + ciphertext);
        scanner.close();*/

        /*HillCipher hillCipher=new HillCipher();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter ciphertext: ");
        String cipher = scanner.nextLine();
        //String message = "ACT";
        System.out.print("Enter key: ");
        String key = scanner.nextLine();
        // Get the key
       // String key = "GYBNQKURP";
        hillCipher.HillCipher(cipher, key);*/

        /*
        Playfair decrypt=new Playfair();
        String encpt;
        System.out.print("Enter Ciphertext: ");
        Scanner scanner= new Scanner(System.in);
        encpt= scanner.nextLine();
        String key;
        System.out.print("Enter Key: ");
        key= scanner.nextLine();
        decrypt.decryptPlayfair(encpt,key);*/
        System.out.println("Basic Encryption ");
        System.out.println("Choose an Encryption:");
        System.out.println("[1] Caesar Cipher");
        System.out.println("[2] Rail Fence Cipher");
        System.out.println("[3] Playfair Cipher");
        System.out.println("[0] End");
        System.out.print("Input here:");
        Scanner sc= new Scanner(System.in);
        Scanner scan= new Scanner(System.in);
        Scanner scan2= new Scanner(System.in);
        Scanner scan3= new Scanner(System.in);
        Scanner scan4= new Scanner(System.in);
        Scanner scan5= new Scanner(System.in);
        int sel=sc.nextInt();
        while(sel!=0){
            if(sel==1){
                CaesarCipher caesar= new CaesarCipher();
                System.out.println("Caesar Cipher");
                System.out.println("Encryption: [1]");
                System.out.println("Decryption: [2]");
                System.out.println("Exit: [0]");
                System.out.print("Input here:");
                int selCaesar= sc.nextInt();
                while (selCaesar!=0) {
                    if (selCaesar == 1) {
                        System.out.print("Enter Plaintext: ");
                        String input = scan.nextLine();
                        System.out.println("\nEncrypt Caesar Cipher:"+caesar.EncryptCaesar(input));
                        break;
                    }
                    else if (selCaesar == 2) {
                        System.out.print("Enter Ciphertext: ");
                        String input = scan.nextLine();
                        System.out.println("\nDecrypt Caesar Cipher: "+caesar.DecryptCaesar(input));
                     break;
                    }
                    else {
                        System.out.println("Invalid Choice");
                    }
                    continue;

                }
                System.out.println("Choose again:");
                System.out.println("[1] Caesar Cipher");
                System.out.println("[2] Rail Fence Cipher");
                System.out.println("[3] Playfair Cipher");
                System.out.println("[0] End");
                System.out.print("Input here:");
                sel=sc.nextInt();
            }

            else if(sel==2){
                RailFenceCipher railFenceCipher= new RailFenceCipher();
                System.out.println("Rail Fence Cipher");
                System.out.println("Encryption: [1]");
                System.out.println("Decryption: [2]");
                System.out.println("Exit: [0]");
                System.out.print("Input here:");
                int selRail= sc.nextInt();
                while (selRail!=0){
                    if(selRail==1){
                        System.out.println("Rail Fence Encryption");
                        System.out.print("Enter plaintext: ");
                        String plainText = scan2.nextLine();
                        System.out.print("Enter number of rails: ");
                        int key = scan2.nextInt();
                        String ciphertext = railFenceCipher.encryptRailFence(plainText, key);
                        System.out.println("Ciphertext: " + ciphertext);
                        break;
                    }
                    else if(selRail==2){
                        System.out.println("Rail Fence Decryption");
                        System.out.print("Enter ciphertext: ");
                        String cipherText = scan3.nextLine();
                        System.out.print("\nEnter number of rails: ");
                        int key = scan3.nextInt();
                        String ciphertext = railFenceCipher.decryptRailFence(cipherText, key);
                        System.out.println("Plaintext: " + ciphertext);
                        break;
                    }
                    else {
                        System.out.println("Invalid Choice");
                    }
                    continue;
                }
                System.out.println("Choose again:");
                System.out.println("[1] Caesar Cipher");
                System.out.println("[2] Rail Fence Cipher");
                System.out.println("[3] Playfair Cipher");
                System.out.println("[0] End");
                System.out.print("Input here:");
                sel=sc.nextInt();
            }
            else if(sel==3){

                System.out.println("Playfair Cipher");
                System.out.println("Encryption: [1]");
                System.out.println("Decryption: [2]");
                System.out.println("Exit: [0]");
                System.out.print("Input here:");
                int selPlayfair= sc.nextInt();
                while(selPlayfair!=0){
                    if(selPlayfair==1){
                        System.out.println("Playfair Encryption");
                        System.out.print("Plaintext: ");
                        String plainText = scan4.nextLine();
                        System.out.print("Key: ");
                        String key = scan5.nextLine();
                        Playfair2 playfair= new Playfair2(key,plainText);
                        playfair.cleanPlayFairKey();
                        playfair.generateCipherKey();
                        String encryptText = playfair.encryptMessage();
                        System.out.println("Playfair Encryption: " + encryptText);
                        break;

                    }
                    else if(selPlayfair==2){
                        Playfair decrypt=new Playfair();
                        String encpt;
                        System.out.print("Enter Ciphertext: ");
                        Scanner scanner= new Scanner(System.in);
                        encpt= scanner.nextLine();
                        String key;
                        System.out.print("Enter Key: ");
                        key= scanner.nextLine();
                        decrypt.decryptPlayfair(encpt,key);
                        System.out.print("\n");
                        break;
                    }
                    else {
                        System.out.println("Invalid Choice");
                    }
                    continue;
                }
                System.out.println("Choose again:");
                System.out.println("[1] Caesar Cipher");
                System.out.println("[2] Rail Fence Cipher");
                System.out.println("[3] Playfair Cipher");
                System.out.println("[0] End");
                System.out.print("Input here:");
                sel=sc.nextInt();
            }
        }
    }
}

