import java.util.Arrays;
import java.util.Scanner;

public class RailFenceCipher {
    public static String encryptRailFence(String plaintext, int key)
    {

        // create the matrix to cipher plain text
        char[][] rail = new char[key][plaintext.length()];

        // filling the rail matrix to distinguish filled
        // spaces from blank ones
        for (int i = 0; i < key; i++)
            Arrays.fill(rail[i], '\n');

        boolean down = false;
        int rowNum = 0;
        int colNum = 0;

        for (int i = 0; i < plaintext.length(); i++) {

            // check the direction of flow
            // reverse the direction if we've just
            // filled the top or bottom rail
            if (rowNum == 0 || rowNum == key - 1)
                down = !down;

            // fill the corresponding alphabet
            rail[rowNum][colNum++] = plaintext.charAt(i);

            // find the next row using direction flag
            if (down)
                rowNum++;
            else
                rowNum--;
        }

        // now we can construct the cipher using the rail
        // matrix
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < key; i++)
            for (int j = 0; j < plaintext.length(); j++)
                if (rail[i][j] != '\n')
                    result.append(rail[i][j]);

        return result.toString();
    }
    public static String decryptRailFence(String cipherText, int key)
    {

        // create the matrix for plain text
        char[][] rail = new char[key][cipherText.length()];

        // filling the rail matrix
        for (int i = 0; i < key; i++)
            Arrays.fill(rail[i], '\n');

        // to find the direction
        boolean down = true;

        int rowNum = 0;
        int colNum = 0;

        // mark the places with '*'
        for (int i = 0; i < cipherText.length(); i++) {
            // check the direction of flow
            if (rowNum == 0)
                down = true;
            if (rowNum == key - 1)
                down = false;

            // place the marker
            rail[rowNum][colNum++] = '*';

            // find the next row using direction flag
            if (down)
                rowNum++;
            else
                rowNum--;
        }

        // now we can construct the fill the rail matrix
        int index = 0;
        for (int i = 0; i < key; i++)
            for (int j = 0; j < cipherText.length(); j++)
                if (rail[i][j] == '*'
                        && index < cipherText.length())
                    rail[i][j] = cipherText.charAt(index++);

        StringBuilder result = new StringBuilder();

        rowNum = 0;
        colNum = 0;
        for (int i = 0; i < cipherText.length(); i++) {
            // check the direction of flow
            if (rowNum == 0)
                down = true;
            if (rowNum == key - 1)
                down = false;

            // place the marker
            if (rail[rowNum][colNum] != '*')
                result.append(rail[rowNum][colNum++]);

            // find the next row using direction flag
            if (down)
                rowNum++;
            else
                rowNum--;
        }
        return result.toString();
    }

}
